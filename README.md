OSGeo4W building machine overall architecture
============================================

The proposed architecture for automatic windows builds is composed of
the following components:
- a Linux machine where Gitlab is installed. This Gitlab instance is responsible of handling Windows
compilation on virtual machines and hosting a dedicated local OSGeo4W repository.
- on this same machine, a virtualbox virtual machine is used as a
reference for builds
- the packaging repository hosts scripts used by the gitlab-ci to
automate compilation
- the Windows reference virtual machine is cloned for each compilation
so that builds start with a fresh new environment.
- if the building script suceeds, a new package is added to the local OSGeo4W repository
- the Windows virtual machine can also be used in a "regular" continuous integration
scenario for the compilation and tests of a given piece of software during its
development phase

In order to setup this infrastructure in a way that is as much reproducible as
possible, Ansible scripts allow to setup both the Linux machine and the
Windows guest installed as a virtual machine. The Ansible script used
for the Windows VM can also be used (with some additional components)
to setup a (possibly physical) machine already running Windows.

Build/Dev environment installation
----------------------------------

The build environment installation via Ansible is detailed in the [following documentation](ansible/readme.md).
