# Ansible provisioning scripts for OSGeo4W CI

This set of Ansibl scripts are able to instal and configure:
- a Gitlab instance on a Linux machine
- VirtualBox with a Windows 10 virtual machine (VM) preconfigured for the compilation of OSGeo4W packages
- all the glue necessary to make the virtual machine manageable by gitlab-ci

## Client-side dependencies

On the "client" side (the machine from where the installation is launched),
you will need to install some packages:

```bash
sudo apt-get install git virtualenv pwgen
```

Ansible is based on Python, so it is recommended to setup a virtual env
in this directory before anything else. This will keep all updated
packages in a local directory which won't conflict with the modules
installed on the system.

Virtual env creation:

    virtualenv venv

Virtual env activation:

    source venv/bin/activate

Now you can install dependencies with:

    pip install -r requirements.txt
    
## Server-side dependencies

Ansible should be able to connect to the target Linux machine using ssh.

An SSH server then have to be installed and running. On a typical Debian-like
system you could do:

```bash
sudo apt-get install openssh-server
```

### Setup

This setup is done with Ansible, in different steps:
- step 1 provisions the Linux machine, configures the VM and its auto-installation and starts it.
- step 2 provisions the Windows machine, once it is up and running.
- step 3 registers the Windows machine to the Gitlab instance and requires a registration token

#### Configure ansible connection:

Have a detailed look at the sample host configuration file `hosts.sample`, copy it to `hosts` and set
all the required configuration parameters.

Ansible will then need a user to connect without password to the target
machine. You should configure the `.ssh/config` file of this user properly.
You could also make sure its public SSH key is authorized on the target machine.

To generate a new pair of keys:
```bash
ssh-keygen
```

In case the Ansible scripts are launched directly on the target machine,
Ansible will need to connect to "localhost". And because we also use "localhost" for the virtual machine
provision, we need a way to pretend the linux machine is different from the Windows machine.

The host found in the `[gitlab]` section should then be an alias to localhost.
One way to create it is to add a line like this to /etc/hosts

```bash
sudo /bin/bash -c 'echo "127.0.0.1  gitlab.local" >> /etc/hosts'
```

Then to make this user authorized to connect to the target machine:
```bash
ssh-copy-id gitlab.local
```

To check that this step is correctly configured, you can test the connection to the host by typing:

```bash
venv/bin/ansible -vvv -i <host_file> -m ping gitlab
```

### CI server (linux + gitlab + virtualbox + bootstrap the vm)

Provisioning the Linux server and boostrap the windows installation with Ansible: 

```bash
ansible-playbook -i <host_file> -e win_username=<windows username> -e win_password=<windows password> play-gitlab.yml -K
```

This will ask first for the 'sudo' password on the Linux machine.

Once this playbook complete, you will need to wait for Gitlab to finish its startup.
The first run takes a bit of time.
In order to follow the startup progress, logs of the gitlab container can be seen with (on the Linux host):
```bash
sudo docker logs -f gitlab
```
Especially, on first run, a line saying "Mapping UID and GID for git:git to 1004:1004" will take time.

After about 20 minutes the next lines like

```
Mapping UID and GID for git:git to 1004:1004
Initializing logdir...
Initializing datadir...
...
2018-07-19 08:57:52,435 **INFO success**: gitlab-workhorse entered RUNNING state, process has stayed up for > than 1 seconds (startsecs)
```

### Windows VM (build machine)

If you run the Ansible script outside of the Linux target host, you will need to open an SSH tunnel
so that Ansible can communicate with the VM through WinRM on your local machine:
```bash
ssh -L 5985:localhost:5985 <user>@<gitlab_hostname>
```

We'll have to authorize some scripts to run in the VM. To be able to do so,
connect to the VM via RDP (or using the GUI). You'll need to click on "yes" on each administrative
prompt that might appear.

Then run:
```bash
ansible-playbook -i <host_file> -e win_username=<windows username> -e win_password=<windows password> play-vm.yml -K
```

**Note**: this script will install a Windows virtual machine based on a given ISO file of installation.
There is also an alternate Ansible playbook named `play-vm-from-ova.yml` that will import a .ova file on disk
in place of the .iso file. It assumes the imported virtual machine is properly configured. **In particular, 
it will assume virtualbox guest additions are installed and compatible with the version of virtualbox installed
by the first part of the Ansible scripts (5.2.6)**

Once this playbook complete, you need to wait for the Windows
installation to finish. You will need to watch for the Windows
installation to complete by a graphical connection to the virtual
machine, either by using the native virtualbox Graphical User
Interface, or, in headless mode, using an RDP client to the virtual
machine.  Note that the virtual machine is installed with the 'root'
user. To launch the GUI, you will then need to launch it with sudo
rights:

```bash
sudo virtualbox &
```

Once the GUI is launched, click on "Show" to see the VM screen.

### Gitlab configuration

Point a browser to the HTTP url of the gitlab instance and log with the root user.

You should then be able to create new groups and projects.

To import in gitlab an existing project hosted on a different git repository, one way to do
is to add the ssh:// url found in the gitlab repository to your local git repository as a
remote git repository and push to it.

For instance:
```
git add remote esg ssh://git@localhost:10022/TTS/tts_qgis.git
```
This will add an alias called 'esg' to your local git repository with which you will be
able to push (here the local master branch to the remote master branch):

```bash
git push esg master:master
```

You will need to authorize your user to push through ssh onto the gitlab instance.
For that, you will have to copy your public ssh key to gitlab.

Do:
```bash
cat ~/.ssh/id_rsa.pub
```
 and copy the content.
Then once connected to the gitlab web interface, go to "Settings" -> "SSH keys". And paste
the content your previously copied before to add a new authorized key.

#### Clone a project from another git instance

To import a project hosted in another git instance, you will have to:
1. create a new project with the gitlab web interface
2. git clone locally the existing git project
3. add a new remote git that points to the gitlab's project to your local project with `git remote add`
4. make sure your ssh public key is known by gitlab, as described above
5. git push to the remote gitlab

### Registering the windows VM to gitlab-runner

This third part requires a "runner registration token" that is randomly generated on each gitlab installation.
There is currently no way to automate the retrieval of this token using the Gitlab API.

So once the previous steps are completed, connect to the gitlab instance through its web interface with the
"root" user and get a runner registration token. Runners can be registered either as shared runners for all the
gitlab projects, as group runners or for a specific project.

To get a registration token for a global "shared" runner, go to the "Admin area" -> "Runners".

Complete your `hosts` file with the runner registration token (variable `runner_registration_token`).

Then run:
```bash
ansible-playbook -i <host_file> -e win_username=<windows username> -e win_password=<windows password> play-register-runner.yml -K
```

Upon completion, the last task called "Public SSH key to add to gitlab-ci" will display the SSH public key that
needs to be registered to the gitlab-ci instance.

### Triggering a build

See the readme.md file in the `osgeo4w-packages/scripts` repository.

