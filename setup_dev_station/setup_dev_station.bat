@echo off
setlocal

net session >nul 2>&1
if %errorLevel% neq 0 (
  echo This script should be run from an administrator shell
  goto :error
)

:: install chocolatey if needed
if not exist C:/ProgramData/chocolatey (
powershell -NoProfile -InputFormat None -ExecutionPolicy Bypass ^
  -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"
)

::======================= configuration ============================

set http_proxy=
:: visualstudio community ?
set install_msvc=n
:: visual studio professional ?
set install_msvc_pro=y

:: where to install osgeo4w & cygwin
set osgeo4w_root=C:\OSGeo4W64
set cygwin_root=C:\cygwin64

:: could be domain+user
set cygwin_user=%username%

::========================= installation rules ======================
::
set chocolatey_bin=C:/ProgramData/chocolatey/bin
::

if %install_msvc_pro%==y (
  set msvc_ver=professional
) else (
  set msvc_ver=community
)
call :install_choco visualstudio2015%msvc_ver% latest "--AdminFile %~dp0AdminDeployment.xml"

call :title "Downloading Windows 10 SDK"
call :download_file ^
  https://download.microsoft.com/download/C/6/6/C6612AD1-8F30-4124-AC9E-01892ADEAC69/windowssdk/winsdksetup.exe ^
  %USERPROFILE%\Downloads\winsdksetup.exe

call :title "Installing Windows 10 SDK"
start /wait %USERPROFILE%\Downloads\winsdksetup.exe /quiet /norestart

call :install_choco dependencywalker latest

call :install_choco wget latest

call :install_choco cmake 3.7.2

call :title "Downloading OSGeo4W setup"
call :download_file ^
  http://download.osgeo.org/osgeo4w/osgeo4w-setup-x86_64.exe ^
  %USERPROFILE%\Downloads\osgeo4w-setup.exe

call :title "Installing dependencies from Osgeo4W"
call :install_osgeo4w setup
call :install_osgeo4w shell
call :install_osgeo4w setuptools

call :install_choco cygwin latest "/InstallDir:%cygwin_root%"

call :title "Make sure home permissions are ok"
%cygwin_root%/bin/chown.exe -R %cygwin_user% /home/%username% 

call :install_choco cyg-get latest

call :title "Install dependencies from Cygwin"
call %chocolatey_bin%/cyg-get.bat git tar flex bison ssh wget

call :title "Install cygwin shortcut"
%osgeo4w_root%\bin\nircmd shortcut "%cygwin_root%/cygwin.bat" "~$folder.desktop$" "Cygwin shell"  "" "%cygwin_root%/cygwin.ico"

call :title "Download git-lfs"
call :download_file ^
  https://github.com/git-lfs/git-lfs/releases/download/v2.4.0/git-lfs-windows-2.4.0.exe ^
  %USERPROFILE%\Downloads\git-lfs-windows-2.4.0.exe

call :title "Install git-lfs"
%USERPROFILE%\Downloads\git-lfs-windows-2.4.0.exe /silent

call :install_choco notepadplusplus latest

call :install_choco unzip latest

call :install_choco 7zip latest

call :install_choco nsis latest

call :install_osgeo4w txt2tags

call :install_choco llvm latest

call :install_choco qtcreator latest

call :install_choco jom latest

call :install_choco doxygen.install latest

::======================== end of rules =============================

goto :eof

:: %1 - name of the package
:: %2 - version
:: %3 - installation parameters (optional)
:install_choco
setlocal enabledelayedexpansion
if [%1]==[] (echo [choco] Missing package name & exit /b 1)
if [%2]==[] (echo [choco] Missing package version & exit /b 1)
set _ver=%2
if "%_ver%" neq "latest" (
  set args=--version %_ver%
)
if [%3] neq [] set "args=!args! -packageParameters %3"
call :title "[choco] Installing %1 (%_ver%)"
C:\programdata\chocolatey\bin\choco install %1 -y %args% || (echo Installing %1 with choco & goto :error)
endlocal
goto :eof

:: %1 - name of the package
:install_osgeo4w
setlocal
set _osgeo4w_proxy_p=
:: add -p parameter to osgeo4w if needed, but without the 'http://' prefix
if defined http_proxy set _osgeo4w_proxy_p=-p %http_proxy:http://=%
call :title "[osgeo4w] Installing %1"
%USERPROFILE%\Downloads\osgeo4w-setup.exe ^
  -g -q -A -R %osgeo4w_root% -a x86_64 -k ^
  -O -s http://osgeo4w-oslandia.com/extra ^
  -P %1 %_osgeo4w_proxy_p% || (echo Problem executing osgeo4w_setup & goto error)
endlocal
goto :eof

:: %1 - url
:: %2 - local file
:download_file
setlocal
if exist "%2" (
if exist "%2.md5" (
    echo Found md5 file, skipping download
    endlocal
	goto :eof
)
)
:: download the file
echo Download ...
::Powershell -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "(New-Object Net.WebClient).DownloadFile('%1','%2')"
c:\programdata\chocolatey\bin\wget.exe %1 -O %2
if %errorlevel% neq 0 (
 echo Error downloading file %1
 goto :error
)
:: creating .md5
PowerShell -Command "(Get-FileHash %2).hash" > %2.md5
endlocal
goto :eof

:title
echo ==== %1
goto :eof

:error
exit /b 1

  
endlocal
